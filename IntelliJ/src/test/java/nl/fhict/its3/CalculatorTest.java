package nl.fhict.its3;

import nl.fhict.its3.util.Calculator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void plus() {
        int term1 = 1;
        int term2 = 2;

        int result = Calculator.plus(term1, term2);
        assertEquals(3, result);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sumData.csv", numLinesToSkip = 1)
    void sum_shouldGenerateExpectedValue(int t1, int t2, int t3){
        int actualResult = Calculator.plus(t1, t2);
        assertEquals(actualResult, t3);
    }
}