package nl.fhict.its3.util;

public class Calculator {
    public static int plus(int term1, int term2) {
        return term1 + term2;
    }

    // Test Jacoco and gitlab runner shell mode
    public static int dummyPlus(int term1, int term2, int term3, int term4) {
        return term1 + term3;
    }
}
